# 贡献指南

## 介绍
感谢你使用 easyX。

以下是关于向 easyX 提交反馈或代码的指南。在向 easyX 提交 issue 或者 PR 之前，请先花几分钟时间阅读以下内容。

## Issue 规范
+ 遇到问题时，请先确认这个问题是否已经在 issue 中有记录或者已被修复。
+ 提 issue 时，请用简短的语言描述遇到的问题，并添加出现问题时的环境和复现步骤。

## 参与开发

按照下面的步骤操作，即可在本地开发 easyX 组件。

### 克隆仓库
```
git clone https://gitee.com/liuming9157/easyx
```

### 使用HBuilderX打开克隆代码

### 目录结构
easyX 采用 UniAPP X 经典目录结构，所有组件在 components 目录下:
```
root
└─ components //easyX所在目录
|   ├─ easy-action-bar     
|   ├─ easy-action-bar-button   
|   ├─ easy-action-bar-icon  
|   ├─ easy-address-list  
|   ├─ easy-col   
|   └─ .... 
├─ pages  
├─ static
├─ App.uvue
├─ main.uts
├─ pages.json
├─ manifest.json
└─ uni.scss


```
### 代码规范
在编写代码时，请注意：
确保代码格式是规范的，使用 `Ctrl+K` 进行代码格式化。

## 提交 Pull Request

### 参考指南
如果你是第一次在 Gitee 上提 Pull Request ，可以阅读下面这两篇文章来学习：

+ [第一次参与开源](https://github.com/firstcontributions/first-contributions/blob/main/translations/README.zh-cn.md)
+ [如何优雅地在 GitHub 上贡献代码](https://segmentfault.com/a/1190000000736629)  

### Pull Request 规范
在提交 Pull Request 时，请注意：

+ 保持你的 PR 足够小，一个 PR 只解决单个问题或添加单个功能。
+ 当新增组件或者修改原有组件时，记得增加或者修改对应的单元测试，保证代码的稳定。
+ 在 PR 中请添加合适的描述，并关联相关的 Issue。

### Pull Request 流程
+ fork 主仓库，如果已经 fork 过，请同步主仓库的最新代码。
+ 基于 fork 后仓库的 master 分支新建一个分支，比如 feature/button_color。
+  Pull Request 到主仓库的 master 分支。
+ Pull Request 会在 Review 通过后被合并到主仓库。
+ 等待 easyX 发布新版本，一般是每周一次。
+ Pull Request 标题格式

### Pull Request 的标题应该遵循以下格式：
```
type(ComponentName?)：commit message
```
示例：

+ docs: fix typo in quickstart
+ build: optimize build speed
+ fix(Button): incorrect style
+ feat(Button): add color prop

可选的类型：

+ fix
+ feat
+ docs
+ perf
+ test
+ types
+ style
+ build
+ chore
+ release
+ refactor
+ breaking change
+ revert:

### 同步最新代码
提 Pull Request 前，请依照下面的流程同步主仓库的最新代码：
```
# 添加主仓库到 remote
git remote add upstream https://gitee.com:liuming9157/easyx

# 拉取主仓库最新代码
git fetch upstream

# 切换至 master 分支
git checkout master

# 合并主仓库代码
git merge upstream/master
```