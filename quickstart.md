# 快速上手  
## 一、下载源码  
### 从Gitee下载  
Gitee下载地址：[https://gitee.com/liuming9157/easyx](https://gitee.com/liuming9157/easyx)
### 从UniAPP插件市场下载  
插件市场下载地址：[https://ext.dcloud.net.cn/plugin?id=15602](https://ext.dcloud.net.cn/plugin?id=15602)  

## 二、引入组件库  
将下载的源码解压，找到components目录，将该目录下的所有内容复制到你的UniAPP X项目下的components目录即可  

## 三、在项目中使用  
在使用各个组件时，可以参考具体的组件文档。