import Theme from 'vitepress/theme'
// import './style/var.css'
import Icon from './components/icon.vue'
import Color from './components/color.vue'

export default {
    ...Theme,
  enhanceApp({ app }) {
    app.component('Icon', Icon)
    app.component('Color', Color)
    
  }
}
