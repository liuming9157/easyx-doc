import { defineConfig } from 'vitepress'

// https://vitepress.dev/reference/site-config
export default defineConfig({
  title: "easyX",
  description: "easyX是一个轻量、可定制的UniAPP-X的UI库和电商业务组件库，可作为官方组件库的补充。easyX随 UniAPP X发展而发展，未来将适配多平台。easyX始终坚持简单好用、易上手，助力APP快速开发。",
  head: [['meta', { name: 'keywords', content: 'easyx,easyXUI,UI,uniapp-x,电商业务,Xshop' }]],
  lastUpdated: true,
  sitemap: {
    hostname: 'https://easyx.satapp.cn'
  },
  themeConfig: {
    // https://vitepress.dev/reference/default-theme-config
    nav: [
      { text: '首页', link: '/' },
      { text: '文档', link: '/intro' },
      { text: '组件', link: '/basic/icon' },
      { text: '模板', link: '/template/home' },
      { text: '定制主题', link: '/theme' },
      { text: '踩坑', link: '/faq' },

    ],

    sidebar: [
      {
        text: '开发指南',
        items: [
          { text: '介绍', link: '/intro' },
          { text: '快速上手', link: '/quickstart' },
          { text: '定制主题', link: '/theme' },
          { text: '爬坑指南', link: '/faq' },
          { text: '发布记录', link: '/changelog' },
          { text: '贡献指南', link: '/contribution' },


        ]
      },
      {
        text: '基础组件',
        items: [
          { text: 'Icon图标', link: '/basic/icon' },
          { text: 'Layout布局', link: '/basic/layout' },
          { text: 'Popup弹出框', link: '/basic/popup' },
          { text: 'Tag标签', link: '/basic/tag' },
          { text: 'Cell单元格', link: '/basic/cell' },
          { text: 'Collapse折叠面板 ', link: '/basic/collapse' },
          { text: 'Stepper步进器', link: '/basic/stepper' },
          { text: 'Calendar日历', link: '/basic/calendar' },




        ]
      },
      {
        text: '导航组件',
        items: [
          { text: 'ActionBar动作栏', link: '/nav/action-bar' },
          { text: 'Grid宫格', link: '/nav/grid' },
          { text: 'IndexBar索引栏', link: '/nav/index-bar' },
          { text: 'NavBar导航栏', link: '/nav/nav-bar' },
          { text: 'Sidebar侧边导航', link: '/nav/sidebar' },
          { text: 'Tab标签页', link: '/nav/tab' },
          { text: 'Tabbar标签栏', link: '/nav/tabbar' },
        ]
      },
      {
        text: '电商业务组件',
        items: [
          { text: 'Coupon优惠券', link: '/shop/coupon' },
          { text: 'AddressList地址列表', link: '/shop/address-list' },
          { text: 'ContactCard联系人卡片', link: '/shop/contact-card' },
          { text: 'SubmitBar提交订单栏', link: '/shop/submit-bar' },
          { text: 'ImageCube图片魔方', link: '/shop/image-cube' },
          { text: 'GoodCard商品卡片', link: '/shop/good-card' },
          { text: 'Sku选择器', link: '/shop/sku' },
          { text: 'Good商品展示', link: '/shop/good' },
        ]
      },
      {
        text: '常用页面模板',
        items: [
          { text: 'Index商城首页', link: '/template/home' },
          { text: 'Cart购物车', link: '/template/cart' },
          { text: 'Category商品分类', link: '/template/category' },
          { text: 'Mine个人中心', link: '/template/mine' },
          { text: 'Order订单页', link: '/template/order' },
          { text: 'Detail商品详情页', link: '/template/detail' },


        ]
      },
    ],

    socialLinks: [
      { icon: 'github', link: 'https://gitee.com/liuming9157/easyx' }
    ],
    footer: {
      message: 'Released under the MIT License.',
      copyright: 'Copyright © 2023-present Jack Liu'
    },
    search: {
      provider: 'local'
    },
    editLink: {
      pattern: 'https://gitee.com/liuming9157/easyx-doc/tree/master/:path',
      text: '在Gitee上修改本页'
    }
    
  }
})
