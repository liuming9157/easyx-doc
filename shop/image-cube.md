# ImageCube图片魔方

## 介绍  
用于展示1张到3张图片。


## 按需引入

将components目录下`easy-image-cube`组件复制到你的项目conponents目录。

## 代码演示

### 基础用法

```html
<easy-image-cube @choose="onClick" :list="list"></easy-image-cube>
```

### 自定义图片个数

```html
<easy-image-cube @choose="onClick" :list="list" :num="1"></easy-image-cube>
<easy-image-cube @choose="onClick" :list="list" :num="2"></easy-image-cube>
```

### 圆角显示

```html
<easy-image-cube @choose="onClick" :list="list" :round="true"></easy-image-cube>
```

### 自定义图片高度

```html
<easy-image-cube @choose="onClick" :list="list" height="200"></easy-image-cube>
```


## API

### Props

| 参数 | 说明 | 类型 | 默认值 |
| --- | --- | --- | --- |
| num | 图片个数 | _number_ | 3 |
| height | 图片高度| _string_ | 150 |
| list | 图片列表| _array_ | - |
| round | 圆角显示| _boolean_ | false |



### Events

| 事件名 | 说明 | 回调参数 |
| --- | --- | --- |
| choose | 按钮点击事件回调 | index: number |
