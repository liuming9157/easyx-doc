# Card商品卡片 
## 介绍  
商品卡片，用于展示商品的图片、价格等信息。    


## 按需引入

将components目录下`easy-good-card`组件复制到你的项目conponents目录。

## 代码演示

### 基础用法  


```html
<easy-good-card thumb="https://fastly.jsdelivr.net/npm/@vant/assets/ipad.jpeg" title="商品名称"  desc="描述信息" price="2.00" :num="2"></easy-good-card>
```

### 营销信息
通过 `origin-price` 设置商品原价。

```html
<easy-good-card thumb="https://fastly.jsdelivr.net/npm/@vant/assets/ipad.jpeg" title="商品名称"  desc="描述信息" price="2.00"  original-price="2.00" :num="2"></easy-good-card>
```

### 步进器模式
通过 `mode` 设置步进器模式，主要用于购物车。

```html
<easy-good-card thumb="https://fastly.jsdelivr.net/npm/@vant/assets/ipad.jpeg" title="商品名称"  price="2.00"  original-price="2.00" :num="2" mode="stepper"></easy-good-card>
``` 
### 自定义内容 
Card 组件提供了多个插槽，可以灵活地自定义内容。

```html
<easy-good-card class="bg-white" thumb="https://fastly.jsdelivr.net/npm/@vant/assets/ipad.jpeg" title="商品名称"
			desc="描述信息" price="2.00" original-price="10.00" :num="2">
	<template #tags>
		<easy-tag :round="true">标签1</easy-tag>
		<easy-tag :round="true">标签2</easy-tag>
	</template>
	<template #footer>
		<easy-tag :round="true">查看物流</easy-tag>
		<easy-tag :round="true">再来一单</easy-tag>
	</template>
</easy-good-card>
```




## API

### Props

| 参数 | 说明 | 类型 | 默认值 |
| --- | --- | --- | --- |
| thumb | 左侧图片 URL | _string_ | - |
| title | 标题| _string_ | - |
| desc | 描述| _string_ | - |
| num |商品数量| _number_ | 1 |
| price |商品价格| _string_ | - |
| original-price |商品原价| _string_ | - |



### Events

| 事件名 | 说明 | 回调参数 |
| --- | --- | --- |
| click | 点击时触发 | event: MouseEvent |
| click-thumb | 点击自定义图片时触发 | event: MouseEvent |


