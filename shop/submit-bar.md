# SubmitBar提交订单栏  
## 介绍  
用于展示订单金额与提交订单。


## 按需引入

将components目录下`easy-submit-bar`组件复制到你的项目conponents目录。

## 代码演示

### 基础用法

```html
<easy-submit-bar :price="30.5" @submit="submit"></easy-submit-bar>
```

### 自定义按钮文字

```html
<easy-submit-bar :price="40" button-text="立即付款" @submit="submit"></easy-submit-bar>
```


## API

### Props

| 参数 | 说明 | 类型 | 默认值 |
| --- | --- | --- | --- |
| price | 金额 | _number_ | 0 |
| button-text | 按钮文字| _string_ | 提交订单 |

### Events

| 事件名 | 说明 | 回调参数 |
| --- | --- | --- |
| submit | 按钮点击事件回调 | price: number |
