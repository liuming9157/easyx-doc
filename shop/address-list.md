# AddressList 地址列表

### 介绍

展示地址信息列表。


## 代码演示

### 基础用法

```html
<easy-address-list
  :list="list"
  @add="onAdd"
  @edit="onEdit"
  @select="onSelect"
></easy-address-list>
```

```ts
<script lant="uts">
export default {
    data(){
         return {
            list:[
                {
                id: '1',
                name: '张三',
                mobile: '13000000000',
                address: '浙江省杭州市西湖区文三路 138 号东方通信大厦 7 楼 501 室',
                },
                {
                id: '2',
                name: '李四',
                mobile: '1310000000',
                address: '浙江省杭州市拱墅区莫干山路 50 号',
                }] as addressItem[],
            chosenAddressId: 1,
        };
    },
    methods:{
        onAdd(){

        },
        onEdit(index:number){

        },
        onSelect(index:number){

        }
    } 
  },
};
</script>
```

## API

### Props

| 参数 | 说明 | 类型 | 默认值 |
| --- | --- | --- | --- |
| list | 地址列表 | _AddressItem[]_ | `[]` |

### Events

| 事件名 | 说明 | 回调参数 |
| --- | --- | --- |
| add | 点击新增按钮时触发 | - |
| edit | 点击编辑按钮时触发 | index: number |
| select | 切换选中的地址时触发 | index: number|

### AddressListAddress 数据结构

| 键名      | 说明               | 类型               |
| --------- | ------------------ | ------------------|
| id        | 每条地址的唯一标识  | _number_           |
| name      | 姓名               | _string_           |
| mobile    | 手机号          | _string_           |
| address   | 详细地址           | _string_           |


### 类型定义

组件导出以下类型定义：

```ts
import type {  AddressItem } from '@/components/easy-address-list/easy-address-list.uvue'
```