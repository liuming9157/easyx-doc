# Good商品展示 
## 介绍  
商品展示，用于展示商品的图片、价格等信息，与good展示形式不同。    


## 按需引入

将components目录下`easy-good`组件复制到你的项目conponents目录。

## 代码演示

### 基础用法  


```html
<easy-good image="https://fastly.jsdelivr.net/npm/@vant/assets/ipad.jpeg" title="商品名称"  desc="描述信息" price="2.00" :num="2"></easy-good>
```






## API

### Props

| 参数 | 说明 | 类型 | 默认值 |
| --- | --- | --- | --- |
| image | 左侧图片 URL | _string_ | - |
| title | 标题| _string_ | - |
| price |商品价格| _string_ | - |



### Events

| 事件名 | 说明 | 回调参数 |
| --- | --- | --- |
| click | 点击时触发 | event: MouseEvent |


