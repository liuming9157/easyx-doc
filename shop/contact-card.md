# ContactCard联系人卡片

### 介绍

以卡片的形式展示联系人信息。

## 按需引入

将components目录下`easy-contact-card`组件复制到你的项目conponents目录。本组件依赖`easy-icon`组件，也需要将该组件引入。



## 代码演示

### 添加联系人

```html
<easy-contact-card type="add" @click="onAdd"></easy-contact-card>
```

### 编辑联系人

```html
<easy-contact-card type="edit" name="张三" mobile="13610107869" @click="onEdit"></easy-contact-card>
```



## API

### Props

| 参数 | 说明 | 类型 | 默认值 |
| --- | --- | --- | --- |
| type | 卡片类型，可选值为add/edit | _string_ | add |
| name | 联系人姓名 | _string_ | - |
| mobile | 联系人手机号 | _string_ | - |
| add-text | 添加时的文案提示 | _string_ | 添加联系人 |


