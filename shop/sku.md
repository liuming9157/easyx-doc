# Sku选择器

## 介绍  
用于商品详情页面选择商品SKU。


## 按需引入

将components目录下`easy-sku`和`easy-stepper`组件复制到你的项目conponents目录。

## 代码演示

### 基础用法

```html
<template>
	<easy-sku v-model:show="show" :good="good" :sku="sku" :value="value" @num-change="onNumChange" @add-cart="addCart" @buy-clicked="buy"></easy-sku>

</template>

<script>
	import type {SkuItem,GoodType} from '../../components/easy-sku/easy-sku.uvue'
	export default {
		data() {
			return {
				show:false,
				value:1,
				good:{id:1,image:'https://fastly.jsdelivr.net/npm/@vant/assets/ipad.jpeg',price:"1200.00"} as GoodType,
				sku:[
					{k:"颜色",v:[{id:1,name:"白色"},{id:2,name:"黑色",imgUrl:"https://alioss-cdn.mzyun.tech/food/swiper2.jpeg"}]},
					{k:"内存",v:[{id:3,name:"8G"},{id:4,name:"16G"}]},
					] as SkuItem[]
			};
		},
		methods:{
			onNumChange(value:number){
				uni.showToast({
					title:'已选择'+value
				})
			},
			addCart(){
				uni.showModal({
					title:"已点击加入购物车"
				})
			},
			buy(){
				uni.showModal({
					title:"已点击立即购买"
				})
			}
		}
	}
</script>
```

### 显示库存
将`hide-stock`属性设为false可以显示库存

```html
	<easy-sku v-model:show="show" :good="good" :sku="sku" :value="value" :hide-stock="true"></easy-sku>
```



## API

### Props

| 参数 | 说明 | 类型 | 默认值 |
| --- | --- | --- | --- |
| v-model:show | 弹出sku选择器 | _boolean_ | false |
| value | 步进器选择的商品数量| _number_ | 1 |
| good | 商品属性| _GoodType_ | - |
| sku | sku属性| _SkuItem[]_ | - |
| list | sku组合属性| _listItem[]_ | - |




### Events

| 事件名 | 说明 | 回调参数 |
| --- | --- | --- |
| add-cart | 加入购物车 | - |
| buy-clicked | 立即购买 | - |
| num-change | 步进器数据改变 | value:number |  

### 类型定义

组件导出以下类型定义：

```ts
import type {  listItem,SkuItem,GoodType } from '@/components/easy-sku/easy-sku.uvue'
```
