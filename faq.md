# 常见踩坑问题  
1. 为什么`font-size`或`color`等css属性无效？  
答：检查自己是否在`Text`组件上使用的这些属性。与js版不同的是，X中`View`组件是容器组件，不可以书写文字，因此关于文字的css属性都不会生效。具体查看[font-size适用组件](https://uniapp.dcloud.net.cn/uni-app-x/css/font-size.html#app%E5%B9%B3%E5%8F%B0%E5%B7%AE%E5%BC%82)  

2. `border`属性无法动态切换？  
答：这是个官方bug，需要等待官方修复。  

3. `flex`为什么默认是纵向排列？
答：在APP中，`flex-direction`默认为`column`,在其他平台，默认为`row`.因此建议开发者显式书写`flex-direction`。