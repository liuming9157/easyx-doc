# Tab 标签页

## 介绍  
选项卡组件，用于在不同的内容区域之间进行切换。   
**该组件为实验性组件，后期可能发生变更** 

## 按需引入

将components目录下`easy-tabs`组件复制到你的项目conponents目录。

## 代码演示

### 基础用法

```html
<template>
    <easy-tabs :list="list" @change="onChange"></easy-tabs>
</template>
<script>
	export default {
		data() {
			return {
				list: ['标签一', '标签二', '标签三', '标签四'] as string[]
			};
		},
		methods:{
			onChange(index:number){
				uni.showToast({
					title:'点击'+index
				})
			}
		}
	}
</script>
```

### 卡片模式
通过`type`设置为卡片模式,默认为`line`线条模式  
```html
<template>
    <easy-tabs :list="list" @change="onChange" type="card"></easy-tabs>
</template>

```

## 自定义颜色  
默认使用了`uni.scss`中的`$uni-color-primary`变量。  
如果您不想修改全局`$uni-color-primary`变量，可以直接修改组件中的`.active`样式  
## API

### Props

| 参数 | 说明 | 类型 | 默认值 |
| --- | --- | --- | --- |
| list | 标签列表 | _array_ | - |
| type | 模式 | _string_ | line |


### Events

| 事件名 | 说明 | 回调参数 |
| --- | --- | --- |
| change | 切换标签事件回调 | index: number |
