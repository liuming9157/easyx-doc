# ActionBar 动作栏

## 介绍
用于为商品详情页面相关操作提供便捷交互。

## 按需引入

将components目录下`easy-action-bar`和`easy-action-bar-icon`和`easy-action-bar-button`三个组件复制到你的项目conponents目录。本组件依赖`easy-icon`组件，也需要将该组件引入。


## 代码演示

### 基础用法

需注意，`easy-action-bar-button`组件需放入button具名插槽中。
```html
<easy-action-bar>
	<easy-action-bar-icon icon="chat-o" text="客服" @click="onClickKefu"></easy-action-bar-icon>
	<easy-action-bar-icon icon="cart-o" text="购物车" @click="onClickCart"></easy-action-bar-icon>
	<easy-action-bar-icon icon="shop-o" text="店铺"  @click="onClickShop"></easy-action-bar-icon>
	<template #button>
	    <easy-action-bar-button color="orange" text="加入购物车" @click="onClickAddCart"></easy-action-bar-button>
	    <easy-action-bar-button color="orangered" text="立即购买" @click="onClickBuy"></easy-action-bar-button>
	</template>
</easy-action-bar>
```

### 自定义按钮颜色
设置`easy-action-bar-button`组件的`color`属性可以自定义按钮颜色

```html
<easy-action-bar>
	<easy-action-bar-icon icon="chat-o" text="客服" @click="onClickKefu"></easy-action-bar-icon>
	<easy-action-bar-icon icon="cart-o" text="购物车" @click="onClickCart"></easy-action-bar-icon>
	<easy-action-bar-icon icon="shop-o" text="店铺"  @click="onClickShop"></easy-action-bar-icon>
	<template #button>
	    <easy-action-bar-button color="#aaaaff" text="加入购物车" @click="onClickAddCart"></easy-action-bar-button>
	    <easy-action-bar-button color="#5500ff" text="立即购买" @click="onClickBuy"></easy-action-bar-button>
	</template>
</easy-action-bar>
```

## API



### ActionBarIcon Props

| 参数   | 说明           | 类型               | 默认值 |
| ------ | -------------- | ------------------ | ------ |
| icon   | 图标            | _string_          | -|
| text   | 图标文字        | _string_          | -      |


### ActionBarButton Props

| 参数   | 说明           | 类型               | 默认值 |
| ------ | -------------- | ------------------| ------ |
| color  | 按钮颜色        | _string_          | orangered|
| text   | 按钮文字        | _string_          | -      |
