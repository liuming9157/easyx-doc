# Grid宫格  

## 介绍
宫格可以在水平方向上把页面分隔成等宽度的区块，用于展示内容或进行页面导航。

## 按需引入

将components目录下`easy-grid`和`easy-grid-item`两个组件复制到你的项目conponents目录。零，本组件依赖`easy-icon`组件，也需要将该组件引入。

## 代码演示

### 基础用法

```html
<easy-grid>
  <easy-grid-item icon="photo-o" text="文字" />
  <easy-grid-item icon="photo-o" text="文字" />
  <easy-grid-item icon="photo-o" text="文字" />
  <easy-grid-item icon="photo-o" text="文字" />
</easy-grid>
```

## 自定义列数 
默认一行展示四个格子，可以通过 `column-num` 自定义列数。
```
<easy-grid :column-num="3">
  <easy-grid-item v-for="value in 6" :key="value" icon="photo-o" text="文字" />
</easy-grid>
```

## 自定义内容  
通过插槽可以自定义格子展示的内容。
```
<easy-grid :column-num="3">
	<easy-grid-item>
		<image class="image" src="https://cdn.mzyun.tech/shop/cate1.jpg" ></image>		
	</easy-grid-item>
	<easy-grid-item>
			<image class="image" src="https://cdn.mzyun.tech/shop/cate2.jpg" ></image>		
	</easy-grid-item>
	<easy-grid-item>
		<image class="image" src="https://cdn.mzyun.tech/shop/cate3.jpg" ></image>		
	</easy-grid-item>
</easy-grid>
```

## API



### Grid Props

| 参数   | 说明           | 类型               | 默认值 |
| ------ | -------------- | ------------------ | ------ |
| column-num| 列数         | _number_          | 4|


### GridItemProps

| 参数   | 说明           | 类型               | 默认值 |
| ------ | -------------- | ------------------| ------ |
| icon  | 图标        | _string_          | -|
| text   | 按钮文字        | _string_          | -      |

