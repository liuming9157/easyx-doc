---
# https://vitepress.dev/reference/default-theme-home-page
layout: home
title: easyX
titleTemplate: 电商业务组件库

hero:
  name: easyX
  text: 电商业务组件库
  tagline: 不仅仅是UI库，更是为UniApp X设计的电商业务组件库，简单易上手，加速你的UniAPP X开发
  image:
    src: /logo.svg
    alt: easyX
  actions:
    - theme: brand
      text: 快速上手
      link: /quickstart
    - theme: alt
      text: 开发文档
      link: /intro

features:
  - title: 组件独立
    details: 组件之间彼此独立，可以按需引入
  - title: 长期支持
    details: 随UniAPP X发展而发展，长期保持更新
  - title: 使用简单
    details: 根据示例代码引入，可根据业务需要自行修改源码
---

<style>
:root {
  --vp-home-hero-name-color: transparent;
  --vp-home-hero-name-background: -webkit-linear-gradient(120deg, #bd34fe 30%, #41d1ff);

  --vp-home-hero-image-background-image: linear-gradient(-45deg, #bd34fe 50%, #47caff 50%);
  --vp-home-hero-image-filter: blur(44px);
}

@media (min-width: 640px) {
  :root {
    --vp-home-hero-image-filter: blur(56px);
  }
}

@media (min-width: 960px) {
  :root {
    --vp-home-hero-image-filter: blur(68px);
  }
}
</style>

