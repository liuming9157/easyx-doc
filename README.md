# 介绍  

本仓库是是easyX的文档仓库，easyX源码可请前往[https://gitee.com/liuming9157/easyx](https://gitee.com/liuming9157/easyx)  

## 关于easyX

easyX 是一个**轻量、可定制的UniAPP X电商业务组件库**，可作为官方组件库的补充。

easyX随 [UniAPP X](https://uniapp.dcloud.net.cn/uni-app-x)发展而发展，未来将适配多平台。

easyX始终坚持简单好用、易上手，希望大家能喜欢。  


## 关于UniAPP X
[UniAPP X](https://uniapp.dcloud.net.cn/uni-app-x/)是DCloud开发的下一代 uni-app，是一个跨平台应用开发引擎。  
在App端，uni-app x 在iOS编译为swift、在Android编译为kotlin。没有使用js引擎、webview，完全达到了原生应用的功能、性能。  
UniAPP X性能媲美原生，且可以直接调用原生模块，是DCloud未来发展的重点，强烈推荐开发者使用X开发新应用。  

## 关于商城模板XShop  
[XShop](https://ext.dcloud.net.cn/plugin?id=16033) 是一个**免费开源**的UniAPP-X电商商城模板，所有组件来自于easyX，是easyX的配套项目，如果开发商城APP,可以拿来即用。  
欢迎大家下载体验。


## 一些注意事项  
1. UniAPP X是否支持所有平台？  
是的，但目前UniAPP X处于一期。可以查看官方[开发计划](https://uniapp.dcloud.net.cn/uni-app-x/#%E4%B8%80%E6%9C%9F%E8%8C%83%E5%9B%B4)。  

## 和js版开发的区别  
1. 类型声明  
X采用强类型声明，习惯TypeScript开发的同学适应起来会快一些，但是也有少量差异，比如官方自定义了UTSJSONObeject和UTSArray类型.在X开发中，尤其要注意对象和数组，如果不进行类型声明一定会报错  
2. 组件差异  
在X版中，文字一定要放到Text组件上，千万不要写在View组件上，因为font-size等属性对View组件不生效。  
3. 调试方法  
X版调试需要使用真机，如果要审查元素，可以使用Android Studio的layout inspector。  


## 交流反馈  
作者：刘明，十年创业老兵   
邮箱：liuming@mzsat.cn  
CSDN:[https://blog.csdn.net/weixin_42553583](https://blog.csdn.net/weixin_42553583)

有任何问题，可以加微信交流 
![企业微信](https://alioss-cdn.mzyun.tech/common/qrcode.jpg)
