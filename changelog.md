# 更新记录  

## 1.0.2 (2023-12-22)  
### 新增页面模板
+ [订单中心模板](https://easyx.satapp.cn/template/order.html)   
+ [商品详情页模板](https://easyx.satapp.cn/template/detail.html)   

## 1.0 (2023-12-21)  
### 新增页面模板
+ [个人中心模板](https://easyx.satapp.cn/template/mine.html)   
+ [购物车模板](https://easyx.satapp.cn/template/cart.html)   
+ [商品分类页模板](https://easyx.satapp.cn/template/category.html)   
**本次新增了版本升级功能，需要依赖以下插件，下载源码后务必安装。安装方式很简单，直接打开插件链接，点击右上角导入项目中即可**  
+ [uni-installApk](https://ext.dcloud.net.cn/plugin?id=15118)    
+ [uni-progressNotification](https://ext.dcloud.net.cn/plugin?id=15842)  



## 0.6 (2023-12-10)  
### 新增页面模板
+ [首页模板](https://easyx.satapp.cn/template/home.html)   
### 新增组件
+ [Sku选择器](https://easyx.satapp.cn/shop/sku.html)    

## 0.5 (2023-12-08)  
### 新增组件
+ [Stepper步进器](https://easyx.satapp.cn/basic/stepper.html)  
+ [Calendar日历](https://easyx.satapp.cn/basic/calendar.html)  
+ [Card商品卡片](https://easyx.satapp.cn/shop/card.html)  



## 0.4 (2023-12-03)  
### 新增组件
+ [Cell单元格](https://easyx.satapp.cn/basic/cell.html)  
+ [Collapse折叠面板](https://easyx.satapp.cn/basic/collapse.html)
+ [Sidebar侧边导航](https://easyx.satapp.cn/nav/sidebar.html)
+ [Tab标签页](https://easyx.satapp.cn/nav/tab.html)  
+ [IndexBar索引栏](https://easyx.satapp.cn/nav/index-bar.html)  
+ [ImageCube图片魔方](https://easyx.satapp.cn/shop/image-cube.html)



## 0.3 (2023-11-29)  
### 新增组件
+ [ContactCard联系人卡片](https://easyx.satapp.cn/shop/contact-card.html)  
+ [SubmitBar提交订单栏](https://easyx.satapp.cn/shop/submit-bar.html)

## 0.2 (2023-11-28)  
### 新增组件
+ [Grid 宫格](https://easyx.satapp.cn/nav/grid.html)  
+ [Tag标签](https://easyx.satapp.cn/basic/tag.html)


## 0.1(2023-11-27)  
### 新增组件
+ [Icon图标](https://easyx.satapp.cn/basic/icon.html) 共240个图标 
+ [Layout布局](https://easyx.satapp.cn/basic/layout.html) layout布局组件
+ [Popup弹出框](https://easyx.satapp.cn/basic/popup.html) 弹出框
+ [ActionBar动作栏](https://easyx.satapp.cn/nav/action-bar.html) 商品详情页动作栏
+ [AddressList地址列表](https://easyx.satapp.cn/shop/address-list.html) 地址列表