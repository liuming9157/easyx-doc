# Cell单元格  

## 介绍
单元格为列表中的单个展示项。

## 按需引入

将components目录下`easy-cell-group`和`easy-cell`组件复制到你的项目conponents目录。`easy-cell`组件依赖`easy-icon`组件，也需要将该组件引入。  
因`easy-cell`可以单独使用，如果不需要`easy-cell-group`可以不引入。


## 代码演示  

### 基础用法
`Cell`可以单独使用，也可以与`CellGroup`搭配使用，`CellGroup`可以为`Cell`提供上下外边框。

``` html
<easy-cell-group>
  <easy-cell title="单元格" value="内容" />
  <easy-cell title="单元格" value="内容" label="描述信息" />
</easy-cell-group>
```

### 展示图标
通过 `icon` 属性在标题左侧展示图标。

``` html
<easy-cell title="单元格" icon="location-o" />
```


### 展示箭头
设置 `is-link` 属性后会在单元格右侧显示箭头，并且可以通过 `arrow-direction` 属性控制箭头方向。

``` html
<easy-cell title="单元格" :is-link="true" />
<easy-cell title="单元格" :is-link="true" value="内容" />
<easy-cell title="单元格" :is-link="true" arrow-direction="down" value="内容" />
```

## API

### Cell Props

| 参数 | 说明 | 类型 | 默认值 |
| --- | --- | --- | --- |
| title | 左侧标题 | _string_ | - |
| value | 右侧内容| _string_ |- |
| label | 标题下方的描述信息| _string_ |- |
| icon |左侧图标名称或图片链接，等同于Icon组件的name属性| _string_ |- |
| is-link |是否展示右侧箭头并开启点击反馈| _boolean_ |false|
| arrow-direction |箭头方向，可选值为 left up down| _string_ |right |





