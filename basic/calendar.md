# Calendar日历

## 介绍  
日历组件用于选择日期。该组件使用DrawApi绘制，渲染速度极快。


## 按需引入

将components目录下`easy-calendar`组件复制到你的项目conponents目录。

## 代码演示

### 基础用法

```html
<easy-calendar :lunar="true" @change="change"></easy-calendar>
```

### 显示农历

```html
<easy-calendar :lunar="true" @change="change"></easy-calendar>
```

### 自定义选中日期颜色

```html
<easy-calendar active-color="#f00"  @change="change"></easy-calendar>
```



## API

### Props

| 参数 | 说明 | 类型 | 默认值 |
| --- | --- | --- | --- |
| lunar | 显示农历 | _boolean_ | false |
| active-color | 选中日期颜色| _string_ | #00f |
| today-color | 今日颜色| _string_ | #f00 |




### Events

| 事件名 | 说明 | 回调参数 |
| --- | --- | --- |
| change | 选择日期回调 | date: string |
