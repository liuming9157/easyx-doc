# Collapse 折叠面板 

## 介绍
将一组内容放置在多个折叠面板中，点击面板的标题可以展开或收缩其内容。

## 按需引入

将components目录下`easy-collapse`和`easy-collapse-item`组件复制到你的项目conponents目录即可。
## 代码演示  

### 基础用法
通过`title`设置标题

``` html
<easy-collapse>
  <easy-collapse-item title="标题1" >
    代码是写出来给人看的，附带能在机器上运行。
  </easy-collapse-item>
  <easy-collapse-item title="标题2" >
    技术无非就是那些开发它的人的共同灵魂。
  </easy-collapse-item>
  <easy-collapse-item title="标题3">
    在代码阅读过程中人们说脏话的频率是衡量代码质量的唯一标准。
  </easy-collapse-item>
</easy-collapse>
```

### 设置展开状态
通过 `open` 属性设置展开状态。

``` html
<easy-collapse>
  <easy-collapse-item title="标题1" :open="true">
    代码是写出来给人看的，附带能在机器上运行。
  </easy-collapse-item>
</easy-collapse>
```


### 设置禁用
通过 `disabled` 属性设置禁用状态。

``` html
<easy-collapse>
  <easy-collapse-item title="标题1" :disabled="true">
    代码是写出来给人看的，附带能在机器上运行。
  </easy-collapse-item>
</easy-collapse>
```

## API

### CollapseItem Props

| 参数 | 说明 | 类型 | 默认值 |
| --- | --- | --- | --- |
| title | 标题 | _string_ | - |
| open |展开状态| _boolean_ |false|
| disabled |禁用状态| _boolean_ |false|







