# Layout 布局

### 介绍

Layout 提供了 `easy-row` 和 `easy-col` 两个组件来进行行列布局。

## 按需引入

将components目录下`easy-row`和`easy-col`两个组件复制到你的项目conponents目录即可。


## 代码演示

### 基础用法

Layout 组件提供了 `24列栅格`，通过在 `Col` 上添加 `span` 属性设置列所占的宽度百分比,添加`offset`属性设置便宜量，添加`gutter`属性添加间距。
```html
<easy-row>
  <easy-col :span="8">span: 8</easy-col>
  <easy-col :span="8">span: 8</easy-col>
  <easy-col :span="8">span: 8</easy-col>
</easy-row>

<easy-row>
  <easy-col :span="4">span: 4</easy-col>
  <easy-col :span="10" :offset="4">offset: 4, span: 10</easy-col>
</easy-row>

<easy-row>
  <easy-col  :span="12" :offset="12">offset: 12, span: 12</easy-col>
</easy-row>

<easy-row>
  <easy-col :span="8" :gutter="4">span: 8，gutter:4</easy-col>
  <easy-col :span="8" :gutter="4">span: 8,gutter:4</easy-col>
  <easy-col :span="8" :gutter="4">span: 8,gutter:4</easy-col>
</easy-row>
```


## API



### Col Props

| 参数   | 说明           | 类型               | 默认值 |
| ------ | -------------- | ------------------ | ------ |
| span   | 列元素宽度     | _number_            | -      |
| offset | 列元素偏移距离 | _number_             | 0      |
| gutter | 列元素间距     | _number_            | 0      |
