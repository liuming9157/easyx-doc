# tag标签

## 介绍
用于标记关键词和概括主要内容。 

## 按需引入

将components目录下`easy-tag`组件复制到你的项目conponents目录即可。

## 代码演示  

### 基础用法
通过 `type` 属性控制标签颜色。默认为primary
```
<easy-tag type="primary">标签</easy-tag>
<easy-tag type="success">标签</easy-tag>
<easy-tag type="error">标签</easy-tag>
<easy-tag type="warning">标签</easy-tag>
```

### 空心样式
通过 `plain` 属性设置为空心样式。注意：一定要显示的书写plain为true，否则会报错
```
<easy-tag :plain="true" type="primary">标签</easy-tag>
```
### 圆角样式
通过 `round` 属性设置为圆角样式。注意：一定要显示的书写round为true，否则会报错
```
<easy-tag :round=“true type="primary">标签</easy-tag>
```

### 自定义颜色
通过 `color` 属性设置标签颜色。
```
<easy-tag color="#50f">标签</easy-tag>
```
## API

### Props

| 参数 | 说明 | 类型 | 默认值 |
| --- | --- | --- | --- |
| type |类型，可选值为 primary success error warning | _string_ | primary |
| plain | 是否为空心样式 | _boolean_ | false |
| round | 是否为圆角样式 | _boolean_ | false |
| color | 自定义标签颜色 | _string_ | - |


