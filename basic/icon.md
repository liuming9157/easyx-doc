# Icon图标 


## 所有图标  
共240个  
<Icon></Icon>

## 按需引入

将components目录下`easy-icon`组件复制到你的项目conponents目录即可。

## 代码演示
### 基础用法
通过 name 属性来指定需要使用的图标，easyX 内置了一套图标库，可以直接传入对应的名称来使用。
```
<easy-icon name="edit" />
```

### 图标颜色
通过 color 属性来设置图标的颜色。
```
<easy-icon name="edit" color="#1989fa" />
<easy-icon name="edit" color="#ee0a24" />
```
### 图标大小
通过 size 属性来设置图标的尺寸大小，可以指定任意 CSS 单位。size属性默认为16px。
```

<!-- 不指定单位，默认使用 px -->
<easy-icon name="edit" size="40" />
<!-- 指定使用 rem 单位 -->
<easy-icon name="edit" size="3rem" />
```

## API

### Props

| 参数 | 说明 | 类型 | 默认值 |
| --- | --- | --- | --- |
| name | 图标名称或图片链接 | _string_ | - |
| color | 图标颜色 | _string_ | #000 |
| size | 图标大小，如 `20px` `2em`，默认单位为 `px` | _number \| string_ | 32px |

### Events

| 事件名 | 说明           | 回调参数            |
| ------ | -------------- | ------------------- |
| click  | 点击图标时触发 | _event: MouseEvent_ |

### 说明
easyX使用的是开源项目Vant的图标库，在此感谢该项目及其开发者。Vant 的所有图标都托管在 iconfont.cn 上，点此查看：[Vant 图标库](https://www.iconfont.cn/collections/detail?cid=31945)。