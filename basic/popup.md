# Popup弹出框

## 介绍
弹出层容器，用于展示弹窗、信息提示等内容，当前仅支持底部弹出。  

## 按需引入

将components目录下`easy-popup`组件复制到你的项目conponents目录即可。

## 代码演示  

### 基础用法
通过   `v-model:show` 控制弹出层是否展示。


```
<easy-popup v-model:show="show">内容</easy-popup>
```

### 设置弹窗高度  
通过 `height` 控制弹出层高度，默认为50%。
```
<easy-popup v-model:show="show" height="70%">内容</easy-popup> 
```

## API

### Props

| 参数 | 说明 | 类型 | 默认值 |
| --- | --- | --- | --- |
| v-model:show | 是否显示弹出层 | _boolean_ | false |
| height | 弹出层高度| _string_ |50% |
