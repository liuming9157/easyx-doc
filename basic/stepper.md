# Stepper步进器  
## 介绍  
步进器由增加按钮、减少按钮和输入框组成，用于在一定范围内输入、调整数字。  


## 按需引入

将components目录下`easy-stepper`组件复制到你的项目conponents目录。

## 代码演示

### 基础用法  

通过 `v-model` 绑定输入值，可以通过 `change` 事件监听到输入值的变化。

```html
<easy-stepper v-model="value1"  ></easy-stepper>
```

### 步长设置  
通过`step` 属性设置每次点击增加或减少按钮时变化的值，默认为`1`。

```html
<easy-stepper v-model="value2" :step="2"  ></easy-stepper>
```
### 限制输入范围  
通过 `min` 和 `max` 属性限制输入值的范围，默认超出范围后会自动校正最大值或最小值。

```html
<easy-stepper v-model="value3" :min="5" :max="8"></easy-stepper>
```

### 自定义大小 
通过 `button-size` 属性设置按钮大小和输入框高度,默认单位是px.

```html
<easy-stepper v-model="value4" button-size="40"></easy-stepper>
```


## API

### Props

| 参数 | 说明 | 类型 | 默认值 |
| --- | --- | --- | --- |
| v-model | 当前输入的值 | _number_ | - |
| min | 最小值| _number_ | 0 |
| max | 最大值| _number_ | 1000 |
| step |步长，每次点击时改变的值| _number_ | 1 |
| button-size |按钮大小以及输入框高度，默认单位为 px| _string_ | 28 |



### Events

| 事件名 | 说明 | 回调参数 |
| --- | --- | --- |
| change | 当绑定值变化时触发的事件 | value: number |

